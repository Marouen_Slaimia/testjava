package fr.isika.testprojet.java;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class TestprojetJavaApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	  @Test
	  public void testSayHi() throws Exception {
	      this.mockMvc.perform(MockMvcRequestBuilders.get("/").param("saisie", "Joe"))
	                  .andExpect(MockMvcResultMatchers.status().isOk())
	                  .andExpect(MockMvcResultMatchers.model().attribute("msg", "Bonjour, Joe."))
	                  .andExpect(MockMvcResultMatchers.view().name("TestSaisie"))
	                  .andDo(MockMvcResultHandlers.print());
	  }
}
